<?php

namespace App\GraphQL\Queries;

use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use Illuminate\Support\Facades\DB;

class FindByUnitOrName
{
    /**
     * Return a value for the field.
     *
     * @param  @param  null  $root Always null, since this field has no parent.
     * @param  array<string, mixed>  $args The field arguments passed by the client.
     * @param  \Nuwave\Lighthouse\Support\Contracts\GraphQLContext  $context Shared between all fields.
     * @param  \GraphQL\Type\Definition\ResolveInfo  $resolveInfo Metadata for advanced query resolution.
     * @return mixed
     */
    public function __invoke($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        return DB::table('empadronamientos')
                    ->join('empresas as em', function ($join) use ($args){
                        $join->on('empadronamientos.IdPadron', '=', 'em.IdPadron')
                             ->where('empadronamientos.idUnidad', '=', $args['idUnidad'])
                             ->where('em.RazonSocial','like','%'.$args['empresa'].'%');
                    })
                    ->select('em.IdPadron','em.RazonSocial')
                    //->count(DB::raw('DISTINCT e.IdPadron'))
                    ->take(20)
                    ->get();
    }
}
