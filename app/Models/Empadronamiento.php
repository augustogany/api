<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Empadronamiento extends Model
{
    use HasFactory;

     //protected $primaryKey = 'InicioTramite';
     protected $table = 'Empadronamientos';
     public $timestamps = false;

     protected $cast = [
        'InicioTramite' => 'datetime'
     ];
}
